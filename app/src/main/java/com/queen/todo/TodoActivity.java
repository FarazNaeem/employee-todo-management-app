package com.queen.todo;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.queen.todo.FragmentAdaptors.HistoryAdaptor;
import com.queen.todo.FragmentAdaptors.TaskPoolAdaptor;
import com.queen.todo.FragmentAdaptors.TodoAdaptor;
import com.queen.todo.Fragments.AdminFragment;
import com.queen.todo.Fragments.AdminHistoryFragment;
import com.queen.todo.Fragments.HistoryFragment;
import com.queen.todo.Fragments.TaskPollFragment;
import com.queen.todo.Fragments.TodoFragment;
import com.queen.todo.Utils.FirebaseHelper;
import com.queen.todo.Utils.TaskPoolModel;
import com.queen.todo.interfaces.AdminListener;
import com.queen.todo.interfaces.taskPoolListener;
import com.queen.todo.interfaces.todoListener;

public class TodoActivity extends AppCompatActivity implements todoListener, taskPoolListener, AdminListener {

    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private ViewPagerAdaptor mViewPagerAdaptor;
    private Button graph;
    private static final int ASSIGN_TASK_REQUEST_CODE=1363;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo);

        __INIT__();
        mViewPagerAdaptor= new ViewPagerAdaptor(getSupportFragmentManager());
        String type =FirebaseHelper.getInstance().getCurrentUser().getEmail();
        System.out.println("user email dasfdf "+type);
        if(type.contains("Admin")|| type.contains("admin")){
            mViewPagerAdaptor.addFragments(new AdminFragment(this),"Admin");
            mViewPagerAdaptor.addFragments(new TaskPollFragment(this),"TASK POOl");
            mViewPagerAdaptor.addFragments(new AdminHistoryFragment(),"History");
        }else{
            mViewPagerAdaptor.addFragments(new TaskPollFragment(this),"TASK POOl");
            mViewPagerAdaptor.addFragments(new TodoFragment(this),"TODO");
            mViewPagerAdaptor.addFragments(new HistoryFragment(),"History");
        }
        mViewPager.setAdapter(mViewPagerAdaptor);
        mTabLayout.setupWithViewPager(mViewPager);
        //        graph.setOnClickListener(new View.OnClickListener() {
        //            @Override
//            public void onClick(View view) {
//                startActivity(new Intent(TodoActivity.this,ProgressActivity.class));
//            }
//        });
//        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
//        UpdateToken();
    }

    private void UpdateToken(){
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {

                        if (!task.isSuccessful()){
                            return;
                        }

                        String Token=task.getResult().getToken();
                        Toast.makeText(TodoActivity.this, "my toked "+Token, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void __INIT__() {
        mTabLayout=findViewById(R.id.tablayout);
        mViewPager=findViewById(R.id.viewpager);
    }

    @Override
    public void todoClick(String documentId) {
        if(documentId.equals("test")){

        }else{
            TodoFirebaseUtils.getInstance().todoCompleted(documentId);
            TodoFirebaseUtils.getInstance().deletetodo(documentId);
        }

    }

    @Override
    public void createHistory(TodoModel model) {
        TodoFirebaseUtils.getInstance().addToHistory(model);
        HistoryAdaptor.addNewhistory("Pending",model.getMyTodo());
        HistoryAdaptor adaptor=HistoryFragment.getHistoryAdaptor();
        adaptor.notifyItemInserted(0);
        adaptor.notifyItemRangeChanged(0,HistoryAdaptor.getHistorySize());

    }
    @Override
    public void optTask(TaskPoolModel model) {
        if(model.getDocumentId().equals("test")){

        }else{
            TodoFirebaseUtils.getInstance().takeTask(model);
            TodoFirebaseUtils.getInstance().incrementTaskOpted();
            TodoAdaptor adaptor=TodoFragment.getTodoAdaptor();
            TodoAdaptor.addMyTodo(model.getMyTodo(),model.getStatus());
            Toast.makeText(this, "documetn id "+model.getDocumentId()+model.getMyTodo(), Toast.LENGTH_LONG).show();
            adaptor.notifyItemInserted(0);
            adaptor.notifyItemRangeChanged(0,TodoAdaptor.getmyTodoSize());
            TodoFirebaseUtils.getInstance().deletePoolTask(model.getDocumentId());
        }

    }

    @Override
    public void onAssignTask() {
        Intent intent = new Intent(TodoActivity.this,AssignTask.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent,ASSIGN_TASK_REQUEST_CODE);
        
    }

    @Override
    public void onCreatePoolTask() {
        Intent intent = new Intent(TodoActivity.this,CreatePoolTask.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent,ASSIGN_TASK_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode== ASSIGN_TASK_REQUEST_CODE){
            Toast.makeText(this, "Task Created Successfully", Toast.LENGTH_SHORT).show();
            if(resultCode == RESULT_OK && data!=null){
                String todo=data.getStringExtra("todo");
                TaskPoolAdaptor adaptor=TaskPollFragment.getTaskPoolAdaptor();

                adaptor.addNewTask(todo);
                adaptor.notifyItemInserted(0);
            }else{
                Toast.makeText(this, "data is null", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(FirebaseAuth.getInstance().getCurrentUser().getEmail().contains("Admin")
        || FirebaseAuth.getInstance().getCurrentUser().getEmail().contains("admin")){
            getMenuInflater().inflate(R.menu.adminmenuu,menu);
        }else{
            getMenuInflater().inflate(R.menu.employeemenu,menu);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.logout:
                FirebaseAuth.getInstance().signOut();
                Intent intent= new Intent(TodoActivity.this,MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;
            case R.id.profile:
                Intent intent1= new Intent(TodoActivity.this,ProfileActivity.class);
                intent1.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent1);
                break;
            default:
                break;
        }
        return true;
    }
}