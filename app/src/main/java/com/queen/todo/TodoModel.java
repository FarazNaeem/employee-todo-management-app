package com.queen.todo;

public class TodoModel {
    private String myTodo;
    private String status;
    private String documentId;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public TodoModel(){}
    public String getMyTodo() {
        return myTodo;
    }

    public void setMyTodo(String myTodo) {
        this.myTodo = myTodo;
    }
}
