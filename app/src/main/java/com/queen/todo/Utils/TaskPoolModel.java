package com.queen.todo.Utils;

import com.google.firebase.firestore.model.ServerTimestamps;

import java.util.Date;

public class TaskPoolModel {

    private String documentId,myTodo;
    private String status;
    private Date date;


    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    public TaskPoolModel(){}

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }


    public String getMyTodo() {
        return myTodo;
    }

    public void setMyTodo(String myTodo) {
        this.myTodo = myTodo;
    }
}
