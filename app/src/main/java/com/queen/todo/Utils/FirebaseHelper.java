package com.queen.todo.Utils;

import com.google.firebase.auth.FirebaseAuth;

public class FirebaseHelper {
    private static FirebaseAuth mAuth;
    public static FirebaseAuth getInstance(){
        if(mAuth ==  null)
            mAuth=FirebaseAuth.getInstance();
        return mAuth;
    }
}
