package com.queen.todo;

import java.util.Date;

public class userTodoModel {
    private String myTodo,by;
    private String userId;



    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMyTodo() {
        return myTodo;
    }

    public void setMyTodo(String myTodo) {
        this.myTodo = myTodo;
    }

    public String getBy() {
        return by;
    }

    public void setBy(String by) {
        this.by = by;
    }
}
