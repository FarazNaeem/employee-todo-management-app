package com.queen.todo.interfaces;

import com.queen.todo.TodoModel;

public interface todoListener {
        void todoClick(String documentId);
        void createHistory(TodoModel model);
}
