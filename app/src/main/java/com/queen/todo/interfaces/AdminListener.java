package com.queen.todo.interfaces;

public interface AdminListener  {
    void onAssignTask();
    void onCreatePoolTask();
}
