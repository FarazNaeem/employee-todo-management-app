package com.queen.todo;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.queen.todo.Utils.TaskPoolModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CreatePoolTask extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {
    private TextInputEditText task;
    private Button createPookTask;
    private TextView dateTv;
    private ImageView dateBtn;
    DatePickerDialog datePickerDialog;
    TaskPoolModel model;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_pool_task);
        __INIT__();
        model= new TaskPoolModel();
        getSupportActionBar().setTitle("Create Pool Task");
        dateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(CreatePoolTask.this,CreatePoolTask.this, Calendar.getInstance().get(Calendar.YEAR),
                        Calendar.getInstance().get(Calendar.MONTH),
                        Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
            }
        });

        createPookTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog alertDialog = new AlertDialog.Builder(CreatePoolTask.this).create();
                alertDialog.setTitle("Alert");
                alertDialog.setMessage("Are you sure to create this task");
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        alertDialog.dismiss();
                    }
                });
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (!task.getText().toString().trim().equals("")) {
                            model.setMyTodo(task.getText().toString().trim());
                            model.setStatus("pending");
                            TodoFirebaseUtils.getInstance().createPoolTask(model);
                            finish();
                        } else {
                            Toast.makeText(CreatePoolTask.this, "task cannot be null", Toast.LENGTH_SHORT).show();

                        }
                        alertDialog.dismiss();
                    }
                });
                alertDialog.show();
            }
        });

    }
    private void showDatePicker(){
         datePickerDialog = new DatePickerDialog(this,this, Calendar.getInstance().get(Calendar.YEAR),
                Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }
    private void __INIT__() {
        task=findViewById(R.id.admin_task_et);
        createPookTask=findViewById(R.id.admin_create_task_btn);
        dateTv=findViewById(R.id.date_tv);
        dateBtn=findViewById(R.id.calender);
    }


    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        dateTv.setText(year+"/"+month+"/"+day);
        String str=day+"/"+month+"/"+year;
        Date date= new Date();
        try {
            date=new SimpleDateFormat("dd/MM/yyyy").parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(date!=null){
            model.setDate(date);
        }
    }
}