package com.queen.todo.Adaptors;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.queen.todo.Models.DashBoardEntryModel;
import com.queen.todo.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class DashBoardAdaptor extends  RecyclerView.Adapter<DashBoardAdaptor.itemViewHolder>{


    private static ArrayList<DashBoardEntryModel> entryModel;
    private Context context;
    public DashBoardAdaptor(Context context,ArrayList<DashBoardEntryModel> entryModel){
        this.context=context;
        this.entryModel=entryModel;
    }
    @NonNull
    @Override
    public itemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.dash_board_recycler_items,parent,false);
        return new itemViewHolder(v);
    }
    public static void updateLastEntryCheckIn(Date date){
        DashBoardEntryModel model= new DashBoardEntryModel();
        model.setDate(date);
        model.setCheckIn(date);
        entryModel.add(model);
    }
    public static void updateLastEntryCheckOut(Date date){
        entryModel.get(entryModel.size()-1).setCheckOut(date);
    }
    @Override
    public void onBindViewHolder(@NonNull itemViewHolder holder, int position) {
        if(entryModel.get(position).getDate() != null){
            SimpleDateFormat dateFormat=new SimpleDateFormat("dd-MM-yyyy");
            holder.date.setText(dateFormat.format(entryModel.get(position).getDate()));
        }else{
            holder.date.setText("NA");
        }
        if(entryModel.get(position).getCheckIn() != null){
            SimpleDateFormat dateFormat=new SimpleDateFormat("hh:mm a");
            holder.checkIn.setText(dateFormat.format(entryModel.get(position).getCheckIn()));
        }else{
            holder.checkIn.setText("NA");
        }
        if(entryModel.get(position).getCheckOut() != null){
            SimpleDateFormat dateFormat=new SimpleDateFormat("hh:mm a");
            holder.chechOut.setText(dateFormat.format(entryModel.get(position).getCheckOut()));
        }else{
            holder.chechOut.setText("NA");
        }
    }

    @Override
    public int getItemCount() {
        if(entryModel== null){
            return  0;
        }
        return entryModel.size();
    }

    public class itemViewHolder extends RecyclerView.ViewHolder{
        private TextView date,checkIn,chechOut;
        public itemViewHolder(@NonNull View itemView) {
            super(itemView);
            date=itemView.findViewById(R.id.dash_date);
            checkIn=itemView.findViewById(R.id.dash_check_in);
            chechOut=itemView.findViewById(R.id.dash_check_out);

        }
    }
}
