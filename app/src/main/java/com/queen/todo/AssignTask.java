package com.queen.todo;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.firestore.auth.User;
import com.queen.todo.Utils.FirebaseHelper;
import com.queen.todo.interfaces.spinnerListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

public class AssignTask extends AppCompatActivity implements spinnerListener, DatePickerDialog.OnDateSetListener {

    private ArrayList<UserNameModel> userNamesList = new ArrayList<>();
    private ArrayAdapter adapter;
    private Spinner spinner;
    ArrayList<String> listt;
    private Button createtask;
    private ArrayList<UserNameModel> userNameModel;
    private TextInputEditText taskDesc;
    private String uid = "abc";
    private ImageView dateBtn;
    private TextView dateTv;
    userTodoModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assign_task);
        __INIT__();
        getSupportActionBar().setTitle("Assign Task");

        createtask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(taskDesc.getText().toString().trim())) {
                    if (!uid.equals("abc")) {
                        TodoFirebaseUtils.getInstance().incrementTodoTask(uid);
                        model = new userTodoModel();
                        model.setBy("Admin");
                        model.setMyTodo(taskDesc.getText().toString().trim());
                        model.setUserId(uid);
//                        model.setDate(new Date());
                        Toast.makeText(AssignTask.this, "uid " + uid, Toast.LENGTH_SHORT).show();
                        System.out.println("Task Added" + uid);
                        TodoFirebaseUtils.getInstance().createUserTodo(model);

//                        Intent intent = new Intent();
//                        setResult(RESULT_OK, intent);
//                        finish();
                    } else {

                        Toast.makeText(AssignTask.this, "please select a valid user", Toast.LENGTH_LONG).show();

                    }
                } else {
                    Toast.makeText(AssignTask.this, "Task Description cannot be null", Toast.LENGTH_LONG).show();
                }
            }
        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                uid = userNameModel.get(i).getDocumentId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                uid = "abc";
            }
        });
        TodoFirebaseUtils.getInstance().getUserNames(this).observe(this, new Observer<ArrayList<UserNameModel>>() {
            @Override
            public void onChanged(ArrayList<UserNameModel> userNameModels) {
                userNameModel = userNameModels;
            }
        });

        dateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(AssignTask.this, AssignTask.this, Calendar.getInstance().get(Calendar.YEAR),
                        Calendar.getInstance().get(Calendar.MONTH),
                        Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
            }
        });
    }

    private void __INIT__() {
        dateTv = findViewById(R.id.date_tv);
        spinner = findViewById(R.id.spinner);
        createtask = findViewById(R.id.admin_create_task_btn);
        taskDesc = findViewById(R.id.admin_task_et);
        dateBtn = findViewById(R.id.calender);
    }

    @Override
    public void onSpinnerLoaded() {
        listt = TodoFirebaseUtils.getInstance().getUserStaringList();
        adapter = new ArrayAdapter<>(AssignTask.this, android.R.layout.simple_spinner_item, listt);
        spinner.setAdapter(adapter);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        dateTv.setText(year + "/" + month + "/" + day);
        String str=day+"/"+month+"/"+year;
        Date date= new Date();
        try {
            date=new SimpleDateFormat("dd/MM/yyyy").parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(date!=null){
        }

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(RESULT_CANCELED, intent);
        super.onBackPressed();
    }
}