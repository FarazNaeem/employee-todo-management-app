package com.queen.todo;


import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;
import com.google.firebase.firestore.auth.User;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.internal.$Gson$Preconditions;
import com.queen.todo.Models.DashBoardEntryModel;
import com.queen.todo.Models.ProfileModel;
import com.queen.todo.Utils.FirebaseHelper;
import com.queen.todo.Utils.TaskPoolModel;
import com.queen.todo.interfaces.spinnerListener;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class TodoFirebaseUtils {
    private static TodoFirebaseUtils instance;
    public static TodoFirebaseUtils getInstance(){
        if(instance ==  null)
            instance= new TodoFirebaseUtils();
        return instance;
    }
    private DocumentSnapshot historyLastUpdate;
    private DocumentSnapshot taskPoolLastUpdate;
    private DocumentSnapshot todoLastUpdate;
    private FirebaseFirestore db=FirebaseFirestore.getInstance();
    private MutableLiveData<ArrayList<TodoModel>> myTodo= new MutableLiveData<>();
    private ArrayList<TodoModel> todoList=new ArrayList<>();
    private DocumentSnapshot documentSnapshot;
    private MutableLiveData<Boolean> isRetriving= new MutableLiveData<>();

    private MutableLiveData<Boolean> changeListener= new MutableLiveData<>();
    private MutableLiveData<Boolean> isPoolRetriving= new MutableLiveData<>();
    private MutableLiveData<ArrayList<TaskPoolModel>> taskPool= new MutableLiveData<>();
    private ArrayList<TaskPoolModel> poolList= new ArrayList<>();
    public MutableLiveData<Boolean> getIsRetriving(){
        return isRetriving;
    }
    private MutableLiveData<ArrayList<HistoryModel>> myHistory= new MutableLiveData<>();
    private ArrayList<HistoryModel> historyList= new ArrayList<>();
    private MutableLiveData<Boolean> isRetrivingHistory= new MutableLiveData<>();
    private ArrayList<AdminHistoryModel> adminHistoryList= new ArrayList<>();
    private MutableLiveData<ArrayList<AdminHistoryModel>> adminHistory= new MutableLiveData<>();
    private MutableLiveData<ArrayList<UserNameModel>> userNames= new MutableLiveData<>();
    private UserNameModel chooseUser= new UserNameModel("Choose a Name");

    private ArrayList<UserNameModel> userNameList= new ArrayList<>();
    private MutableLiveData<ProfileModel> myProfile= new MutableLiveData<>();
    private MutableLiveData<Boolean> isGettingProfile= new MutableLiveData<>();
    private MutableLiveData<ArrayList<DashBoardEntryModel>> myEntries= new MutableLiveData<>();
    private ArrayList<DashBoardEntryModel> entryList= new ArrayList<>();
    private MutableLiveData<Boolean> isGettingEntries= new MutableLiveData<>();
    private MutableLiveData<Boolean> gettingUserName=new MutableLiveData<>();
    private MutableLiveData<Boolean> isUpdatingProfileImage= new MutableLiveData<>();
    public MutableLiveData<ArrayList<TaskPoolModel>> getTaskPool(){
        System.out.println("sex task lene tu aya" );
        Query query;
        if(taskPoolLastUpdate == null){
            query= db.collection("pooltask")
                    .limit(5);
        }else{
            query=db.collection("pooltask")
                    .startAfter(taskPoolLastUpdate)
                    .limit(5);
        }
        isPoolRetriving.setValue(true);
        query
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        TaskPoolModel model;
                        for (DocumentSnapshot d:queryDocumentSnapshots.getDocuments()) {
                            model=d.toObject(TaskPoolModel.class);
                            System.out.println(d);
                            model.setDocumentId(d.getId());
                            poolList.add(model);
                        }
                        if(queryDocumentSnapshots.size() >0){
                            taskPoolLastUpdate=queryDocumentSnapshots.getDocuments().get(queryDocumentSnapshots.size()-1);
                        }
                        taskPool.postValue(poolList);

                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("FirebaseUtils", "onFailure: ",e );
            }
        }).addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                isPoolRetriving.postValue(false);
                taskPool.setValue(poolList);
            }
        });
        taskPool.setValue(poolList);
        return taskPool;
    }
    public MutableLiveData<ArrayList<HistoryModel>> getMyHistory(){
        Query query;
        isRetrivingHistory.setValue(true);
        if(historyLastUpdate== null){
         query=db.collection("users").document(FirebaseHelper.getInstance().getCurrentUser().getUid())
                 .collection("history")
                 .limit(5);
        }else{
        query= db.collection("users").document(FirebaseHelper.getInstance().getCurrentUser().getUid())
                .collection("history")
                .limit(5)
                .startAfter(historyLastUpdate);
        }
                query
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        HistoryModel model;
                        for (DocumentSnapshot d:queryDocumentSnapshots.getDocuments()) {
                            model=d.toObject(HistoryModel.class);
                            historyList.add(model);
                        }
                        if(queryDocumentSnapshots.size() >0){
                            historyLastUpdate=queryDocumentSnapshots.getDocuments().get(queryDocumentSnapshots.size()-1);
                        }
                        isRetrivingHistory
                                .postValue(false);
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("FirebaseUtils", "onFailure: ", e);
            }
        }).addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                isRetrivingHistory.postValue(false);
                myHistory.setValue(historyList);

            }
        });
        myHistory.setValue(historyList);
        return myHistory;
    }
    public MutableLiveData<Boolean> getIsRetrivingHistory(){
        return isRetrivingHistory;
    }
    public ArrayList<String> getUserStaringList(){
        ArrayList<String> newList= new ArrayList<>();
        for(int i=0;i<userNameList.size();i++){
            newList.add(i,userNameList.get(i).getName());
        }
        return newList;
    }
    public MutableLiveData<ArrayList<UserNameModel>> getUserNames(final spinnerListener listener){
        userNameList= new ArrayList<>();
        if(!userNameList.contains(chooseUser)){
            userNameList.add(0,chooseUser);
        }
        gettingUserName.setValue(true);
        db.collection("users")
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        UserNameModel model;
                        for (DocumentSnapshot d:queryDocumentSnapshots.getDocuments()) {
                            model=d.toObject(UserNameModel.class);
                            model.setDocumentId(d.getId());
                            userNameList.add(model);
                        }
                    }
                }).addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                userNames.setValue(userNameList);
                gettingUserName.postValue(false);
                listener.onSpinnerLoaded();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("sex getusername", "onFailure: ",e );
            }
        });
        userNames.setValue(userNameList);
        return this.userNames;
    }
    public MutableLiveData<Boolean> getGettingUserName(){

        return  gettingUserName;
    }
    public void createUserTodo(userTodoModel model){
        db.collection("users")
                .document(model.getUserId())
                .collection("todo")
                .document()
                .set(model)
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("sex fail", "onFailure: admin pool task fail",e );
                    }
                }).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

            }
        });
    }
    public void createPoolTask(TaskPoolModel model){
        model.setStatus("pending");
        db.collection("pooltask")
                .document()
                .set(model)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("FirebaseUtils", "onFailure: ",e );
            }
        });
    }
    public MutableLiveData<ArrayList<AdminHistoryModel>> getAdminHistory(){
        db.collection("adminHistory")
                .limit(5)
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        AdminHistoryModel model;
                        for (DocumentSnapshot d:queryDocumentSnapshots.getDocuments()) {
                            model=d.toObject(AdminHistoryModel.class);
                            adminHistoryList.add(model);
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("FirbaseUtils", "onFailure: ", e);
            }
        }).addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                adminHistory.setValue(adminHistoryList);
            }
        });
        adminHistory.setValue(adminHistoryList);
        return adminHistory;
    }
    public void addToHistory(TodoModel model){
        db.collection("users")
                .document(FirebaseHelper.getInstance().getCurrentUser().getUid())
                .collection("history")
                .document(model.getDocumentId())
                .set(model).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("FirebaseUtils", "onFailure: ",e );
            }
        });
    }
    public void deletetodo(String documentId){
        db.collection("users")
                .document(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .collection("todo")
                .document(documentId)
                .delete()
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });
    }
    public void deletePoolTask(String documentId){
        db.collection("pooltask")
                .document(documentId)
                .delete()
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });
    }
    public MutableLiveData<ArrayList<TodoModel>> getMyTodo(){
        Query query;
        if(todoLastUpdate ==  null){
            query=db.collection("users")
                    .document(FirebaseHelper.getInstance().getCurrentUser().getUid())
                    .collection("todo")
                    .limit(5);
        }else{
            query=db.collection("users")
                    .document(FirebaseHelper.getInstance().getCurrentUser().getUid())
                    .collection("todo")
                    .startAfter(todoLastUpdate)
                    .limit(5);
        }
        if(FirebaseHelper.getInstance()== null){
            return null;
        }
        isRetriving.setValue(true);
                query
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        TodoModel model= new TodoModel();
                        for (DocumentSnapshot d:queryDocumentSnapshots.getDocuments()) {
                            model=d.toObject(TodoModel.class);
                            model.setDocumentId(d.getId());
                            todoList.add(model);
                            System.out.println("sex raka "+d.getId());
                        }
                        if(queryDocumentSnapshots.size() >0){
                            todoLastUpdate=queryDocumentSnapshots.getDocuments().get(queryDocumentSnapshots.size()-1);
                        }
                        myTodo.setValue(todoList);
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("FirebaseUtils", "onFailure: retrival of data is ",e );
            }
        }).addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                myTodo.setValue(todoList);
                isRetriving.postValue(false);
            }
        });


        myTodo.setValue(todoList);
        return myTodo;
    }
    public void todoCompleted(String documentId){
        Map<String,Object> map= new HashMap<>();
        map.put("task","completed");
        map.put("review","pending");
        db.collection("users").document(FirebaseHelper.getInstance().getCurrentUser().getUid())
                .collection("todo")
                .document(documentId)
                .set(map, SetOptions.merge())
                .addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("FirebaseUtils", "onFailure: ",e );
            }
        });
    }
    public MutableLiveData<Boolean> getChangeListener(){
        return changeListener;
    }
    public void takeTask(TaskPoolModel model){
        db.collection("users")
                .document(FirebaseHelper.getInstance().getCurrentUser().getUid())
                .collection("todo")
                .document(model.getDocumentId())
                .set(model);
    }
    public MutableLiveData<Boolean> getIsPoolRetriving(){
        return isPoolRetriving;
    }
    public MutableLiveData<ProfileModel> getMyProfile(){
        isGettingProfile.setValue(true);
        db.collection("users")
                .document(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        System.out.println("sex " +documentSnapshot);
                        myProfile.setValue(documentSnapshot.toObject(ProfileModel.class));
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("sex", "onFailure: getting profile",e );
            }
        }).addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                isGettingProfile.postValue(false);
            }
        });
        return myProfile;
    }
    public MutableLiveData<Boolean> getIsGettingProfile(){
        return isGettingProfile;
    }

    public MutableLiveData<ArrayList<DashBoardEntryModel>> getMyEntries(){
        isGettingEntries.setValue(true);
        entryList= new ArrayList<>();

        db.collection("users")
                .document(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .collection("entries")
                .limit(10)
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        DashBoardEntryModel model;
                        for (DocumentSnapshot d:queryDocumentSnapshots.getDocuments()) {
                            model=d.toObject(DashBoardEntryModel.class);
                            entryList.add(model);
                        }
                        myEntries.setValue(entryList);
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("sex", "onFailure: getting entries", e);
            }
        }).addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                isGettingEntries.postValue(false);
            }
        });
        return myEntries;

    }
    public MutableLiveData<Boolean> getIsGettingEntries(){
        return isGettingEntries;
    }

    public void addCheckIn(){
        Map<String,Object> map= new HashMap<>();
        Date date= new Date();
        SimpleDateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd");
        map.put("checkIn", date);
        map.put("date",date);
        db.collection("users")
                .document(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .collection("entries")
                .document(dateFormat.format(date))
                .set(map,SetOptions.merge())
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("six,,,,,,,", "onFailure: entry check in ", e);
                    }
                });
    }

    public void addCheckOut(){
        Map<String,Object> map= new HashMap<>();
        //LocalDate date= LocalDate.now();
        SimpleDateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd");
        Date time=new Date();
        map.put("checkOut", time);
        db.collection("users")
                .document(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .collection("entries")
                .document(dateFormat.format(time))
                .set(map,SetOptions.merge())
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("sex", "onFailure: entry check in ", e);
                    }
                });

    }
    public void incrementTaskOpted(){
        db.collection("users")
                .document(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .update("mytasks", FieldValue.increment(1))
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("sex", "onFailure: incrementing task", e);
                    }
                });
    }
    public void incrementTodoTask(String uid){
        db.collection("users")
                .document(uid)
                .update("mytasks", FieldValue.increment(1))
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("sex", "onFailure: incrementing task", e);
                    }
                });
    }
    public void UploadProfileStorage(Uri uri){
        isUpdatingProfileImage.setValue(true);
        final StorageReference childref= FirebaseStorage.getInstance().getReference()
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(System.currentTimeMillis()+".jpg");
        final UploadTask task=childref.putFile(uri);
        Task<Uri> taske=task.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {

                    if(!task.isSuccessful()){
                        return null;
                    }

                    return childref.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                isUpdatingProfileImage.postValue(false);
                updateDatabaseUrl(task.getResult().toString());
            }
        });



    }
    public MutableLiveData<Boolean> getIsUpdatingProfileImage(){
        return isUpdatingProfileImage;
    }
    private void updateDatabaseUrl(String url){
        Map<String,Object> map= new HashMap<>();
        map.put("imageUrl",url);
        db.collection("users")
                .document(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .set(map,SetOptions.merge())
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("sex", "onFailure: upload profile database",e );
                    }
                });

    }
}
