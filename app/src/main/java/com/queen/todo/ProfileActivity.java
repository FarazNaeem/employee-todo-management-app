package com.queen.todo;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.Observer;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.queen.todo.Models.ProfileModel;
import com.queen.todo.Utils.UniversalImageLoader;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity {

    private ImageLoader imageLoader;
    private CircleImageView circleImageView;
    private TextView myTasks,hoursSpent,averageTaskTime,designation,approvedTasks,joiningDate,username;
    private TextView email,change_profile;
    private ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        __INIT__();
        TodoFirebaseUtils.getInstance().getMyProfile().observe(this, new Observer<ProfileModel>() {
            @Override
            public void onChanged(ProfileModel profileModel) {
                setUpProfile(profileModel);
            }
        });
        TodoFirebaseUtils.getInstance().getIsGettingProfile().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if(!aBoolean){
                    progressBar.setVisibility(View.GONE);
                }else{
                    progressBar.setVisibility(View.VISIBLE);
                }
            }
        });

        change_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(CheckStoragePermission()){
                    PickImageIntent();
                }
            }
        });
    }
    private void PickImageIntent(){
        Intent intent= new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(intent,393);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode ==  393){
            if(data!= null){

                Uri uri=data.getData();
                TodoFirebaseUtils.getInstance().UploadProfileStorage(uri);
                Toast.makeText(this, "Profile Picture Changed", Toast.LENGTH_SHORT).show();
                imageLoader.displayImage(String.valueOf(uri),circleImageView);

                TodoFirebaseUtils.getInstance().getIsUpdatingProfileImage().observe(ProfileActivity.this, new Observer<Boolean>() {
                    @Override
                    public void onChanged(Boolean aBoolean) {
                        if(!aBoolean){
                            progressBar.setVisibility(View.GONE);
                        }else{
                            progressBar.setVisibility(View.VISIBLE);
                        }
                    }
                });
            }
        }
    }
    private boolean CheckStoragePermission(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if(ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE ) != PackageManager.PERMISSION_GRANTED){
                return true;
            }else{
                ActivityCompat.requestPermissions(ProfileActivity.this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},3323);
            }
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == 3323){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                PickImageIntent();
            }
        }
    }

    private void setUpProfile(ProfileModel profileModel){

        imageLoader.displayImage(profileModel.getImageUrl(),circleImageView);
        username.setText(profileModel.getName());
        if(FirebaseAuth.getInstance().getCurrentUser().getEmail()!= null){
            email.setText(FirebaseAuth.getInstance().getCurrentUser().getEmail());
        }

        myTasks.setText(profileModel.getMytasks()+" ");
        averageTaskTime.setText(profileModel.getAveragetasktime()+"h");
        hoursSpent.setText(profileModel.getHoursspent()+"h");
        approvedTasks.setText(profileModel.getApprovedtask()+" ");
        designation.setText(profileModel.getDesignation());
    }
    private void __INIT__() {
        imageLoader=ImageLoader.getInstance();
        change_profile=findViewById(R.id.change_profile);
        circleImageView=findViewById(R.id.profile_image);
        username=findViewById(R.id.username);
        email=findViewById(R.id.email);
        myTasks=findViewById(R.id.my_task_pro);
        averageTaskTime=findViewById(R.id.average_time);
        approvedTasks=findViewById(R.id.approved_tasks);
        hoursSpent=findViewById(R.id.hours_spent);
        designation=findViewById(R.id.designation);
        joiningDate=findViewById(R.id.joining_date);
        progressBar=findViewById(R.id.progress2);
        initImageLoader();
    }
        private void initImageLoader() {
            UniversalImageLoader universalImageLoader=new UniversalImageLoader(getApplicationContext());
            ImageLoader.getInstance().init(universalImageLoader.getConfig());

        }
}