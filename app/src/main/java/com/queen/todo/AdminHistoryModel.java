package com.queen.todo;

public class AdminHistoryModel {
    private String name,myTodo,startDate,doneDate;
    public AdminHistoryModel(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMyTodo() {
        return myTodo;
    }

    public void setMyTodo(String myTodo) {
        this.myTodo = myTodo;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getDoneDate() {
        return doneDate;
    }

    public void setDoneDate(String doneDate) {
        this.doneDate = doneDate;
    }
}
