package com.queen.todo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.queen.todo.Utils.FirebaseHelper;

public class MainActivity extends AppCompatActivity {

    private TextInputEditText username,password;
    private Button login;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        __INIT__();
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login.setEnabled(false);
                progressBar.setVisibility(View.VISIBLE);
                LoginUser();
            }
        });
    }
    private void LoginUser(){
        if(TellyCredentials()){

                FirebaseHelper.getInstance().signInWithEmailAndPassword(username.getText().toString().trim(),password.getText().toString().trim())
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if(task.isSuccessful()){
                                    if(FirebaseAuth.getInstance().getCurrentUser().getEmail().contains("Admin")||
                                    FirebaseAuth.getInstance().getCurrentUser().getEmail().contains("admin")){
                                        Intent intent= new Intent(MainActivity.this,DashBoard.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                        finish();                                        
                                    }else{
                                        Intent intent= new Intent(MainActivity.this,DashBoard.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                        finish();
                                    }

                                }else{
                                    login.setEnabled(true);
                                    progressBar.setVisibility(View.GONE);
                                    Toast.makeText(MainActivity.this, "Something went wrong try again", Toast.LENGTH_LONG).show();
                                }
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        login.setEnabled(true);
                        progressBar.setVisibility(View.GONE);
                    }
                });
            }else{
            progressBar.setVisibility(View.GONE);
            login.setEnabled(true);
        }

    }
    private boolean TellyCredentials(){
        if(TextUtils.isEmpty(username.getText().toString().trim())){
            username.setError("username cannot be null");
            return false;
        }
        if(!username.getText().toString().trim().contains("@gapdynamics")){
            username.setError("email is badly formatted");
            return false;
        }
        if(TextUtils.isEmpty(password.getText().toString().trim())){
            password.setError("password cannot be null");
            return false;
        }
        if(password.getText().toString().trim().length() < 6){
            password.setError("wrong password");
            return false;
        }
        return  true;
    }
    private void __INIT__() {
        progressBar=findViewById(R.id.progress);
        username=findViewById(R.id.username);
        password=findViewById(R.id.password);
        login=findViewById(R.id.login);
    }
}