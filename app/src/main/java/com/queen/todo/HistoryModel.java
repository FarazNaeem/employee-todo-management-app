package com.queen.todo;


public class HistoryModel {
    private String myTodo,status;
    private String documentId;

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public HistoryModel(){}

    public String getMyTodo() {
        return myTodo;
    }

    public void setMyTodo(String myTodo) {
        this.myTodo = myTodo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
