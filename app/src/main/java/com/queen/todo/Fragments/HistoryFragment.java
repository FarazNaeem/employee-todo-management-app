package com.queen.todo.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.queen.todo.FragmentAdaptors.HistoryAdaptor;
import com.queen.todo.FragmentAdaptors.TodoAdaptor;
import com.queen.todo.R;
import com.queen.todo.TodoFirebaseUtils;
import com.queen.todo.interfaces.todoListener;


public class HistoryFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private RecyclerView historyRecycler;
    private static HistoryAdaptor historyAdaptor;
    private ProgressBar progressBar;
    private Button complete;
    private SwipeRefreshLayout swipeRefreshLayout;
    public HistoryFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v= inflater.inflate(R.layout.fragment_history, container, false);
        historyRecycler=v.findViewById(R.id.history_recycler);
        swipeRefreshLayout=v.findViewById(R.id.swiprefresh);
        historyRecycler.setLayoutManager(new LinearLayoutManager(container.getContext()));
        progressBar=v.findViewById(R.id.history_progress);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                TodoFirebaseUtils.getInstance().getMyHistory();
            }
        });
        historyAdaptor=new HistoryAdaptor(TodoFirebaseUtils.getInstance().getMyHistory().getValue(),getActivity());
        historyRecycler.setAdapter(historyAdaptor);
        TodoFirebaseUtils.getInstance().getIsRetrivingHistory().observe(getActivity(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if(!aBoolean){
                    swipeRefreshLayout.setRefreshing(false);
                    historyAdaptor.notifyDataSetChanged();
                    progressBar.setVisibility(View.GONE);
                    historyAdaptor.notifyDataSetChanged();
                }else{
                    progressBar.setVisibility(View.VISIBLE);
                }
            }
        });
        return v;
    }
    public static HistoryAdaptor getHistoryAdaptor(){
        return historyAdaptor;
    }
}