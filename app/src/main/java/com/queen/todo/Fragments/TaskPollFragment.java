package com.queen.todo.Fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.queen.todo.FragmentAdaptors.TaskPoolAdaptor;
import com.queen.todo.FragmentAdaptors.TodoAdaptor;
import com.queen.todo.R;
import com.queen.todo.TodoFirebaseUtils;
import com.queen.todo.interfaces.taskPoolListener;


public class TaskPollFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private RecyclerView taskPoolRecycler;
    private static TaskPoolAdaptor taskPoolAdaptor;
    private ProgressBar progressBar;
    private com.queen.todo.interfaces.taskPoolListener taskPoolListener;
    private Context context;
    private SwipeRefreshLayout swipeRefreshLayout;
    public TaskPollFragment(com.queen.todo.interfaces.taskPoolListener taskpoolListener) {
        // Required empty public constructor
        this.taskPoolListener=taskpoolListener;
    }


    // TODO: Rename and change types and number of parameters
//    public static TaskPollFragment newInstance(String param1, String param2) {
//        TaskPollFragment fragment = new TaskPollFragment();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
//        return fragment;
//    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    public static TaskPoolAdaptor getTaskPoolAdaptor(){
        return taskPoolAdaptor;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v= inflater.inflate(R.layout.fragment_todo, container, false);
        taskPoolRecycler =v.findViewById(R.id.todo_recycler);
        swipeRefreshLayout=v.findViewById(R.id.swiprefresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                TodoFirebaseUtils.getInstance().getTaskPool();
            }
        });
        taskPoolRecycler.setLayoutManager(new LinearLayoutManager(container.getContext()));
        progressBar=v.findViewById(R.id.todo_progress);
        taskPoolAdaptor=new TaskPoolAdaptor(TodoFirebaseUtils.getInstance().getTaskPool().getValue(),getActivity(),taskPoolListener);
        taskPoolRecycler.setAdapter(taskPoolAdaptor);

        TodoFirebaseUtils.getInstance().getIsPoolRetriving().observe(getActivity(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if(!aBoolean){
                    swipeRefreshLayout.setRefreshing(false);
                    progressBar.setVisibility(View.GONE);
                    taskPoolAdaptor.notifyDataSetChanged();
                }else{
                    progressBar.setVisibility(View.VISIBLE);
                }
            }
        });
        return v;
    }
}