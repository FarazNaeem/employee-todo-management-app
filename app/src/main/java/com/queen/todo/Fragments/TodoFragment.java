package com.queen.todo.Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.queen.todo.FragmentAdaptors.TodoAdaptor;
import com.queen.todo.R;
import com.queen.todo.TodoFirebaseUtils;
import com.queen.todo.interfaces.todoListener;


public class TodoFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private RecyclerView todoRecycler;
    private static TodoAdaptor todoAdaptor;
    private ProgressBar progressBar;
    private todoListener todoListener;
    private SwipeRefreshLayout swipeRefreshLayout;
    public TodoFragment(todoListener todoListener) {
        // Required empty public constructor
        this.todoListener=todoListener;
    }

    // TODO: Rename and change types and number of parameters
//    public static TodoFragment newInstance(String param1, String param2) {
//        TodoFragment fragment = new TodoFragment();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
//        return fragment;
//    }
    public static TodoAdaptor getTodoAdaptor(){
        return todoAdaptor;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v= inflater.inflate(R.layout.fragment_todo, container, false);
        todoRecycler=v.findViewById(R.id.todo_recycler);
        swipeRefreshLayout=v.findViewById(R.id.swiprefresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                TodoFirebaseUtils.getInstance().getMyTodo();
            }
        });
        todoRecycler.setLayoutManager(new LinearLayoutManager(container.getContext()));
        progressBar=v.findViewById(R.id.todo_progress);
        todoAdaptor=new TodoAdaptor(TodoFirebaseUtils.getInstance().getMyTodo().getValue(),getActivity(),todoListener);
        todoRecycler.setAdapter(todoAdaptor);

        TodoFirebaseUtils.getInstance().getIsRetriving().observe(getActivity(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if(!aBoolean){
                    swipeRefreshLayout.setRefreshing(false);
                    progressBar.setVisibility(View.GONE);
                    todoAdaptor.notifyDataSetChanged();
                }else{
                    progressBar.setVisibility(View.VISIBLE);
                }
            }
        });
        return v;
    }
}