package com.queen.todo.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.queen.todo.CreatePoolTask;
import com.queen.todo.R;
import com.queen.todo.interfaces.AdminListener;


public class AdminFragment extends Fragment {

    private Button createPoolTask,assignTask;
    private AdminListener adminListener;
    public AdminFragment(AdminListener adminListener) {
        // Required empty public constructor
        this.adminListener=adminListener;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v=inflater.inflate(R.layout.fragment_admin, container, false);
        createPoolTask=v.findViewById(R.id.create_pool_task);
        assignTask=v.findViewById(R.id.assgin_task);
        createPoolTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adminListener.onCreatePoolTask();
            }
        });
        assignTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adminListener.onAssignTask();
            }
        });
        return v;
    }
}