package com.queen.todo;


public class UserNameModel {
    String name;
    String documentId;

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public String getName() {
        return name;
    }
    public UserNameModel(String name){
        this.name=name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public  UserNameModel(){}
}
