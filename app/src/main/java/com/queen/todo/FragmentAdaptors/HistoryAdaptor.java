package com.queen.todo.FragmentAdaptors;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.queen.todo.HistoryModel;
import com.queen.todo.R;
import com.queen.todo.TodoModel;

import java.util.ArrayList;

public class HistoryAdaptor extends RecyclerView.Adapter<HistoryAdaptor.myViewHolder>{
    private static ArrayList<HistoryModel> myHistory= new ArrayList<>();
    private Context context;
    public static int getHistorySize(){
       if(myHistory.size()>0){
           return myHistory.size();
       }
        return 0;
    }
    public static void addNewhistory(String status,String myTodo){
        HistoryModel model= new HistoryModel();
        model.setMyTodo(myTodo);
        model.setStatus(status);
        myHistory.add(0,model);
    }
    public HistoryAdaptor(ArrayList<HistoryModel> myHistory, Context context) {
        this.myHistory=myHistory;
        this.context=context;
    }
    @NonNull
    @Override
    public myViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(context).inflate(R.layout.history_items,parent,false);
        return new myViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull myViewHolder holder, int position) {
        holder.myTodoo.setText(myHistory.get(position).getMyTodo()+" ");
        holder.status.setText(myHistory.get(position).getStatus()+" ");
    }

    @Override
    public int getItemCount() {
        return myHistory.size();
    }

    public class myViewHolder extends RecyclerView.ViewHolder{
        private TextView myTodoo,status;
        public myViewHolder(@NonNull View itemView) {
            super(itemView);
            myTodoo=itemView.findViewById(R.id.my_task);
            status=itemView.findViewById(R.id.history_status);
        }
    }
}
