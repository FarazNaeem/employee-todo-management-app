package com.queen.todo.FragmentAdaptors;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.queen.todo.R;
import com.queen.todo.Utils.TaskPoolModel;
import com.queen.todo.interfaces.taskPoolListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class TaskPoolAdaptor extends RecyclerView.Adapter<TaskPoolAdaptor.itemViewHolder> {
    private Context context;
    private ArrayList<TaskPoolModel> taskPool;
    private com.queen.todo.interfaces.taskPoolListener taskPoolListener;

    public TaskPoolAdaptor(ArrayList<TaskPoolModel> taskPool, Context context, taskPoolListener taskPoolListener) {
        this.taskPool = taskPool;
        this.taskPoolListener = taskPoolListener;
        this.context = context;
    }

    @NonNull
    @Override
    public itemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.task_poll_item, parent, false);
        return new itemViewHolder(v);
    }

    public void addNewTask(String todo) {
        TaskPoolModel model = new TaskPoolModel();
        model.setMyTodo(todo);

    }

    @Override
    public void onBindViewHolder(@NonNull final itemViewHolder holder, final int position) {
        holder.task.setText(taskPool.get(position).getMyTodo());
        if (taskPool.get(position).getDate() != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            holder.startDate.setText(dateFormat.format(taskPool.get(position).getDate()) + "");
        }
        holder.taskOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int newPosition = holder.getAdapterPosition();


                try {
                    taskPool.remove(newPosition);
                    notifyItemRemoved(newPosition);
                    notifyItemRangeChanged(newPosition, taskPool.size());
                    taskPoolListener.optTask(taskPool.get(position));
                } catch (IndexOutOfBoundsException e) {
                    taskPool.clear();
                    notifyDataSetChanged();
                }


            }
        });
    }

    @Override
    public int getItemCount() {
        return taskPool.size();
    }

    public class itemViewHolder extends RecyclerView.ViewHolder {
        private Button taskOption;
        private TextView task, startDate;

        public itemViewHolder(@NonNull View itemView) {
            super(itemView);
            taskOption = itemView.findViewById(R.id.task_option);
            task = itemView.findViewById(R.id.task_live);
            startDate = itemView.findViewById(R.id.todo_start_date);
        }
    }
}
