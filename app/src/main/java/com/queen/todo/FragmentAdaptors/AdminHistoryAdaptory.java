package com.queen.todo.FragmentAdaptors;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.queen.todo.AdminHistoryModel;

import java.util.ArrayList;

public class AdminHistoryAdaptory extends RecyclerView.Adapter<AdminHistoryAdaptory.ItemViewHolder>{

    private Context context;
    private ArrayList<AdminHistoryModel> adminHistoryModel;
    public AdminHistoryAdaptory(Context context,ArrayList<AdminHistoryModel> adminHistoryModel){
        this.adminHistoryModel=adminHistoryModel;
        this.context=context;
    }
    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View v= LayoutInflater.from(context).inflate()

        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return adminHistoryModel.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder{

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
