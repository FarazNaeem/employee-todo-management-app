package com.queen.todo.FragmentAdaptors;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.queen.todo.HistoryModel;
import com.queen.todo.R;
import com.queen.todo.TodoDetail;
import com.queen.todo.TodoModel;
import com.queen.todo.Utils.TaskPoolModel;
import com.queen.todo.interfaces.todoListener;

import java.util.ArrayList;

public class TodoAdaptor extends RecyclerView.Adapter<TodoAdaptor.myViewHolder>{
    private static ArrayList<TodoModel> myTodo = new ArrayList<>();
    private Context context;
    private com.queen.todo.interfaces.todoListener todoListener;
    public static int getmyTodoSize(){
        return myTodo.size();
    }
    public static void addMyTodo(String task,String status){
        TodoModel model= new TodoModel();
        model.setMyTodo(task);
        model.setStatus(status);
        myTodo.add(0,model);
    }
    public TodoAdaptor(ArrayList<TodoModel> myTodo, Context context, todoListener todoListener) {
        this.myTodo =myTodo;
        this.context=context;
        this.todoListener=todoListener;
    }
    @NonNull
    @Override
    public myViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(context).inflate(R.layout.todo_items,parent,false);
        return new myViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull final myViewHolder holder, final int position) {
        holder.myTodoo.setText(myTodo.get(position).getMyTodo());
        holder.completed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                todoListener.todoClick(myTodo.get(position).getDocumentId());
                int newPosition=holder.getAdapterPosition();

                try{

                    myTodo.remove(newPosition);
                    notifyItemRemoved(newPosition);
                    notifyItemRangeChanged(newPosition,myTodo.size());
                    todoListener.createHistory(myTodo.get(position));

                }catch (IndexOutOfBoundsException e){
                    myTodo.clear();
                    notifyDataSetChanged();
                }


            }
        });
    }

    @Override
    public int getItemCount() {
        return myTodo.size();
    }

    public class myViewHolder extends RecyclerView.ViewHolder{
        private TextView myTodoo,comment;
        private ImageView completed;
        public myViewHolder(@NonNull final View itemView) {
            super(itemView);
            myTodoo=itemView.findViewById(R.id.my_todo);
         //   comment=itemView.findViewById(R.id.comment);
            completed=itemView.findViewById(R.id.completed);
            myTodoo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent= new Intent(itemView.getContext(),TodoDetail.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    itemView.getContext().startActivity(intent);
                }
            });
        }
    }

}
