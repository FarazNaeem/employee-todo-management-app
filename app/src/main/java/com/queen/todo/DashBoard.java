package com.queen.todo;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.queen.todo.Adaptors.DashBoardAdaptor;
import com.queen.todo.Models.DashBoardEntryModel;
import com.queen.todo.Models.ProfileModel;
import com.queen.todo.Utils.UniversalImageLoader;

import java.util.ArrayList;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

public class DashBoard extends AppCompatActivity {

    private Button myReport,todo;
    private RecyclerView recyclerView;
    private DashBoardAdaptor adaptor;
    private ProgressBar progressBar;
    private Button checkIn,checkOut;
    private Toolbar toolbar;
    private TextView username,designation;
    private CircleImageView profileImage;
    private ImageLoader imageLoader;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dash_board);
        __INIT__();
        setSupportActionBar(toolbar);
        myReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        todo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DashBoard.this,TodoActivity.class));
            }
        });
        TodoFirebaseUtils.getInstance().getMyProfile().observe(this, new Observer<ProfileModel>() {
            @Override
            public void onChanged(ProfileModel profileModel) {
                setUpProfile(profileModel);
            }
        });
        TodoFirebaseUtils.getInstance().getMyEntries().observe(this, new Observer<ArrayList<DashBoardEntryModel>>() {
            @Override
            public void onChanged(ArrayList<DashBoardEntryModel> entryModels) {
                setUpRecycler(entryModels);
            }
        });
        TodoFirebaseUtils.getInstance().getIsGettingEntries().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if(!aBoolean){
                    progressBar.setVisibility(View.GONE);
                }else{
                    progressBar.setVisibility(View.VISIBLE);
                }
            }
        });
        checkIn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View view) {
                TodoFirebaseUtils.getInstance().addCheckIn();
                DashBoardAdaptor.updateLastEntryCheckIn(new Date());
                adaptor.notifyDataSetChanged();
            }
        });
        checkOut.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View view) {
                TodoFirebaseUtils.getInstance().addCheckOut();
                DashBoardAdaptor.updateLastEntryCheckOut(new Date());
                adaptor.notifyDataSetChanged();
            }
        });
    }
    private void setUpProfile(ProfileModel profileModel){
        imageLoader.displayImage(profileModel.getImageUrl(),profileImage);
        username.setText(profileModel.getName());
        designation.setText(profileModel.getDesignation());
    }
    private void setUpRecycler(ArrayList<DashBoardEntryModel> entryModels) {
        adaptor= new DashBoardAdaptor(DashBoard.this,entryModels);
        recyclerView.setLayoutManager(new LinearLayoutManager(DashBoard.this));
        recyclerView.setAdapter(adaptor);
    }

    private void __INIT__() {
        profileImage=findViewById(R.id.profile_image);
        username=findViewById(R.id.dash_username);
        designation=findViewById(R.id.designation);
        toolbar=findViewById(R.id.toolbar);
        progressBar=findViewById(R.id.progress);
        recyclerView=findViewById(R.id.dash_board_recycler);
        myReport=findViewById(R.id.dash_report);
        todo=findViewById(R.id.dash_todo);
        checkIn=findViewById(R.id.check_in);
        checkOut=findViewById(R.id.check_out);
        initImageLoader();
        imageLoader=ImageLoader.getInstance();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.todo_activity:
                Intent intent= new Intent(DashBoard.this,TodoActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.dashboardmenu,menu);
        return super.onCreateOptionsMenu(menu);
    }
    private void initImageLoader() {
        UniversalImageLoader universalImageLoader=new UniversalImageLoader(getApplicationContext());
        ImageLoader.getInstance().init(universalImageLoader.getConfig());

    }
}