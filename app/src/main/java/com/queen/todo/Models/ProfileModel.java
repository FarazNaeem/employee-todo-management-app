package com.queen.todo.Models;

import java.util.Date;

public class ProfileModel {
    String name,designation,imageUrl;
    int mytasks,approvedtask;
    double averagetasktime,hoursspent;
    Date date;
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesignation() {
        return designation;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getApprovedtask() {
        return approvedtask;
    }

    public void setApprovedtask(int approvedtask) {
        this.approvedtask = approvedtask;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public int getMytasks() {
        return mytasks;
    }

    public void setMytasks(int mytasks) {
        this.mytasks = mytasks;
    }


    public double getAveragetasktime() {
        return averagetasktime;
    }

    public void setAveragetasktime(double averagetasktime) {
        this.averagetasktime = averagetasktime;
    }

    public double getHoursspent() {
        return hoursspent;
    }

    public void setHoursspent(double hoursspent) {
        this.hoursspent = hoursspent;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }


}
